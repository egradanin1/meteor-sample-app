import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom';
import routes from '../lib/routes.js';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import '../imports/startup/accounts-config.js';

Meteor.startup(() => {
	Meteor.call('readit', function(err, response) {
		console.log(response);
	});
	render(<BrowserRouter>{routes}</BrowserRouter>, document.getElementById('render-target'));
});
