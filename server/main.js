import { Meteor } from 'meteor/meteor';

import { Movies } from '../imports/api/movies.js';
import { UserData } from '../imports/api/usersdata.js';
import { Messages } from '../imports/api/messages.js';
import { TypingUsers } from '../imports/api/typingusers.js';

Meteor.startup(() => {
	if (!Movies.find().count()) {
		let movies = JSON.parse(Assets.getText('movies.json'));
		movies.forEach(movie => Movies.insert({ ...movie }));
	}
});
