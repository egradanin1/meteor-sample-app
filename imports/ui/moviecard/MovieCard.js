import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './MovieCard.css';

class MovieCard extends Component {
	toggleFavorite() {
		Meteor.call('movies.setFavorite', this.props._id, !this.props.isFavorite, (error, result) =>
			console.log(error, result)
		);
	}
	render() {
		const styleClass = this.props.isFavorite ? 'unfavoriteButton' : 'favoriteButton';
		return (
			<div className="movie-item">
				<Link to={`/details/${this.props._id}`}>
					<img alt={this.props.title} src={this.props.image} />
					<div className="basic-info">
						<h3>{this.props.title}</h3>
						<h4>({this.props.publishedYear})</h4>
					</div>
				</Link>
				<div className="favorite">
					<p>{this.props.description}</p>
					<button className={styleClass} onClick={e => this.toggleFavorite()}>
						{this.props.isFavorite ? 'Unfavorite' : 'Favorite'}
					</button>
				</div>
			</div>
		);
	}
}

export default MovieCard;
