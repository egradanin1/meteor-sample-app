import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import StarRatingComponent from 'react-star-rating-component';
import Header from '../header/Header';
import Review from '../review/Review';
import './MovieInfo.css';
//import axios from 'axios';

import { withTracker } from 'meteor/react-meteor-data';
import { Movies } from '../../api/movies.js';

class MovieInfo extends Component {
	state = {
		movie: null,
		user: null,
		comment: '',
		reviews: [],
		rating: 0
	};
	handleInput(e) {
		this.setState({ comment: e.target.value });
	}
	onStarClick(nextValue, prevValue, name) {
		this.setState({ rating: nextValue });
	}
	handleClick() {
		let { movie } = this.props;
		let { comment, rating } = this.state;
		const review = { movieId: movie._id, username: Meteor.user().username, rating, comment };
		Meteor.call(
			'movies.addReview',
			review,
			error => (error ? error => alert(error) : this.setState({ rating: 0, comment: '' }))
		);
	}
	render() {
		if (this.props.isLoading) return <div>Loading!!!</div>;
		let movie = this.props.movie;
		let reviews = movie.reviews;
		let { comment, rating } = this.state;
		return (
			<div>
				<Header />
				<div className="details">
					<div className="info">
						<img src={movie.image} alt={movie.title} />
						<div className="title">
							<h3>{movie.title}</h3>
							<h4>({movie.publishedYear})</h4>
						</div>
						<p>{movie.description}</p>
					</div>
					<div className="reviews">
						<div hidden={!reviews.length} className="comments">
							<p>
								{reviews && reviews.length
									? (reviews.reduce((acc, el) => acc + el.rating, 0) / reviews.length).toFixed(2) +
									  '/5'
									: ''}
							</p>
							<div className="reviewsBox">
								{reviews.length &&
									reviews
										.slice()
										.reverse()
										.map((el, index) => <Review key={index} {...el} />)}
							</div>
						</div>

						<div className="add-review">
							<input
								value={comment}
								onChange={e => this.handleInput(e)}
								type="text"
								placeholder="Add comment"
							/>
							<StarRatingComponent
								name="rate1"
								starCount={5}
								value={rating}
								onStarClick={this.onStarClick.bind(this)}
							/>
							<button onClick={e => this.handleClick()}>Add review</button>
						</div>
					</div>

					<div className="trailer">
						<iframe
							title="YouTube Video Frame"
							src={`https://www.youtube-nocookie.com/embed/${
								movie.video
							}?rel=0&amp;controls=0&amp;showinfo=0`}
							frameBorder="0"
							allowFullScreen
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default withTracker(props => {
	let subscription = Meteor.subscribe('movies');
	let isLoading = !subscription.ready();
	let movieId = props.match.params.id;

	return {
		movie: Movies.findOne(movieId),
		isLoading
	};
})(MovieInfo);
