import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link, Redirect } from 'react-router-dom';

import MovieCard from '../moviecard/MovieCard.js';
import Header from '../header/Header';
import './Dashboard.css';

import { withTracker } from 'meteor/react-meteor-data';
import { Movies } from '../../api/movies.js';

class Dashboard extends Component {
	state = {
		filter: ''
	};
	handleInput(e) {
		this.setState({
			filter: e.target.value
		});
	}

	renderMovies() {
		let movies = this.props.movies;
		let filter = this.state.filter;
		let filteredMovies;
		this.state.filter
			? (filteredMovies = movies.filter(movie => ~movie.title.toLowerCase().indexOf(filter.toLowerCase())))
			: (filteredMovies = movies);

		return filteredMovies.map((movie, index) => {
			const isFavorite = !!~movie.users.indexOf(Meteor.userId());
			return <MovieCard {...movie} isFavorite={isFavorite} key={movie._id} id={index} />;
		});
	}

	render() {
		let { isLoading, currentUser } = this.props;
		let { filter } = this.state;
		if (!currentUser) {
			return <Redirect to="/login" />;
		}
		if (this.props.isLoading) return <div>Loading!!!</div>;
		return (
			<div>
				<Header />
				<input
					type="text"
					value={filter}
					id="filter-input"
					onChange={e => this.handleInput(e)}
					placeholder="Search"
				/>
				<div className="movie-list">{this.renderMovies()}</div>
			</div>
		);
	}
}

export default withTracker(() => {
	let subscriptionMovies = Meteor.subscribe('movies');
	let isLoading = !subscriptionMovies.ready();

	return {
		movies: Movies.find().fetch(),
		currentUser: Meteor.user(),
		isLoading
	};
})(Dashboard);
