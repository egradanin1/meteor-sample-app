import React, { Component } from 'react';
import './Review.css';
import StarRatingComponent from 'react-star-rating-component';

class Review extends Component {
	render() {
		return (
			<div className="review-info">
				<p>
					{this.props.username}: <span>{this.props.comment}</span>
				</p>
				<StarRatingComponent
					name={this.props.comment + this.props.username}
					starCount={5}
					editable={false}
					value={this.props.rating}
				/>
			</div>
		);
	}
}

export default Review;
