import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Message from '../message/Message';
import MessageInput from '../messageinput/MessageInput';
import './ChatContainer.css';
import Header from '../header/Header';

import { withTracker } from 'meteor/react-meteor-data';
import { Movies } from '../../api/movies.js';
import { Messages } from '../../api/messages.js';
import { TypingUsers } from '../../api/typingusers.js';

class ChatContainer extends Component {
	updateTypingInChat = isTyping => {
		let { currentUser, typingusers } = this.props;
		if (isTyping && !typingusers.some(u => u.username === currentUser.username)) {
			Meteor.call(
				'typingusers.insert',
				currentUser.username,
				error => (error ? alert(error) : console.log('ok'))
			);
		} else if (!isTyping && typingusers.some(u => u.username === currentUser.username)) {
			Meteor.call(
				'typingusers.delete',
				currentUser.username,
				error => (error ? alert(error) : console.log('ok'))
			);
		}
	};

	sendMessage = message => {
		let { currentUser } = this.props;
		Meteor.call(
			'messages.insert',
			{
				message,
				sender: currentUser.username,
				time: `${new Date(Date.now()).getHours()}:${('0' + new Date(Date.now()).getMinutes()).slice(-2)}`
			},
			error => (error ? alert(error) : console.log('ok'))
		);
	};

	sendTyping = isTyping => {
		this.updateTypingInChat(isTyping);
	};

	render() {
		const { currentUser, movies, messages, isLoading, typingusers } = this.props;
		if (isLoading) return <div>Loading!!!</div>;
		if (!currentUser) {
			return <Redirect to="/login" />;
		}
		return (
			<div>
				<Header />
				<div className="container">
					<div className="chat-room-container">
						<Message messages={messages} user={currentUser} typingUsers={typingusers} />
						<MessageInput
							sendMessage={message => {
								this.sendMessage(message);
							}}
							sendTyping={isTyping => {
								this.sendTyping(isTyping);
							}}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default withTracker(() => {
	let subscriptionMovies = Meteor.subscribe('movies');
	let subscriptionMessages = Meteor.subscribe('messages');
	let subscriptionTypingUsers = Meteor.subscribe('typingusers');

	let isLoading = !subscriptionMovies.ready() || !subscriptionMessages.ready() || !subscriptionTypingUsers.ready();

	return {
		movies: Movies.find().fetch(),
		messages: Messages.find().fetch(),
		currentUser: Meteor.user(),
		typingusers: TypingUsers.find().fetch(),
		isLoading
	};
})(ChatContainer);
