import React, { Component } from 'react';
import './Message.css';

class Message extends Component {
	scrollDown = () => {
		const { container } = this.refs;
		if (container) container.scrollTop = container.scrollHeight;
	};

	componentDidMount() {
		this.scrollDown();
	}

	componentDidUpdate(prevProps, prevState) {
		this.scrollDown();
	}

	render() {
		const { messages, user, typingUsers } = this.props;
		if (!messages || !user || !typingUsers) return <div>Loading!!!</div>;
		return (
			<div ref="container" className="thread-container">
				<div className="thread">
					{messages.map(mes => {
						return (
							<div
								key={mes._id}
								className={`message-container ${mes.sender === user.username && 'right'}`}
							>
								<div className="time">{mes.time}</div>
								<div className="data">
									<div className="message">{mes.message}</div>
									<div className="name">{mes.sender}</div>
								</div>
							</div>
						);
					})}
					{typingUsers.filter(u => u.username !== user.username).map(user => {
						return (
							<div key={user._id} className="typing-user">
								{`${user.username} is typing . . .`}
							</div>
						);
					})}
				</div>
			</div>
		);
	}
}
export default Message;
