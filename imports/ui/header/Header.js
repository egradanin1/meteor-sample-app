import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import { Meteor } from 'meteor/meteor';
import './Header.css';

class Header extends Component {
	state = {
		toLogin: false
	};
	handleClick() {
		Meteor.logout();
		this.setState({ toLogin: true });
	}
	render() {
		if (this.state.toLogin) return <Redirect to="/login" />;
		return (
			<header className="App-header">
				<nav>
					<ul id="menu">
						<li>
							<Link to="/">Trending Movie</Link>
						</li>
						<li>
							<Link to="/topmovies">Top Movies</Link>
						</li>
						<li>
							<Link to="/chat">Chat</Link>
						</li>
						<span hidden={!Meteor.user()}>{Meteor.user() && Meteor.user().username}</span>
						<button onClick={e => this.handleClick()}>Logout</button>
					</ul>
				</nav>
			</header>
		);
	}
}

export default Header;
