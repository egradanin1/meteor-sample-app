import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import MovieCard from '../moviecard/MovieCard';
import Header from '../header/Header';
import './TopMovies.css';

import { withTracker } from 'meteor/react-meteor-data';
import { Movies } from '../../api/movies.js';

class TopMovies extends Component {
	renderTopThreeMovies() {
		let movies = this.props.movies;
		let copyOfMovies = JSON.parse(JSON.stringify(movies));
		function averageRating(movie) {
			return (movie.reviews.reduce((acc, el) => acc + el.rating, 0) / movie.reviews.length).toFixed(2);
		}
		copyOfMovies.sort((movieX, movieY) => averageRating(movieY) - averageRating(movieX));

		return copyOfMovies.slice(0, 3).map((movie, index) => {
			const isFavorite = !!~movie.users.indexOf(Meteor.userId());
			return <MovieCard {...movie} isFavorite={isFavorite} key={movie._id} id={index} />;
		});
	}
	render() {
		if (this.props.isLoading) return <div>Loading!!!</div>;
		if (!this.props.currentUser) {
			return <Redirect to="/login" />;
		}
		return (
			<div>
				<Header />
				<div className="movie-list">{this.renderTopThreeMovies()}</div>
			</div>
		);
	}
}

export default withTracker(() => {
	let subscription = Meteor.subscribe('movies');
	let isLoading = !subscription.ready();

	return {
		movies: Movies.find({}).fetch(),
		currentUser: Meteor.user(),
		isLoading
	};
})(TopMovies);
