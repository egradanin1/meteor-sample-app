import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const TypingUsers = new Mongo.Collection('typingusers');

if (Meteor.isServer) {
	Meteor.publish('typingusers', function typingUsersPublication() {
		return TypingUsers.find({});
	});
}

Meteor.methods({
	'typingusers.insert'(typingUser) {
		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}
		TypingUsers.insert({ username: typingUser });
	},
	'typingusers.delete'(typingUser) {
		const typingUserId = TypingUsers.findOne({ username: typingUser })._id;

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		TypingUsers.remove(typingUserId);
	}
});
