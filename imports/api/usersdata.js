import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';

export const UsersData = new Mongo.Collection('usersdata');

if (Meteor.isServer) {
	Meteor.publish('usersdata', function tasksPublication() {
		return UsersData.find({});
	});
}

Meteor.methods({
	'userdata.insert'(user) {
		check(user, {
			email: String,
			username: String,
			password: String,
			profile: { name: { first: String, last: String } }
		});

		UsersData.insert(user);
		Accounts.createUser(user);
	}
});
