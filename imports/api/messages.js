import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Messages = new Mongo.Collection('messages');

if (Meteor.isServer) {
	Meteor.publish('messages', function messagesPublication() {
		return Messages.find({});
	});
}

Meteor.methods({
	'messages.insert'(messageInfo) {
		let { message, time, sender } = messageInfo;
		check(message, String);
		check(time, String);
		check(sender, String);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Messages.insert({
			message,
			time,
			sender
		});
	}
});
