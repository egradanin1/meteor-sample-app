import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Movies = new Mongo.Collection('movies');

if (Meteor.isServer) {
	Meteor.publish('movies', function moviesPublication() {
		return Movies.find({});
	});
}

Meteor.methods({
	'movies.insert'(movie) {
		let { title, description, image, video, publishedYear } = movie;
		check(title, String);
		check(description, String);
		check(image, String);
		check(video, String);
		check(publishedYear, String);

		// allowed to access without login because of the demo purpose

		Movies.insert({
			title,
			description,
			image,
			video,
			publishedYear
		});
	},
	'movies.setFavorite'(movieId, setFavorite) {
		check(movieId, String);
		check(setFavorite, Boolean);

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		setFavorite
			? Movies.update(movieId, { $push: { users: this.userId } })
			: Movies.update(movieId, { $pull: { users: this.userId } });
	},
	'movies.addReview'(review) {
		let { movieId, rating, comment, username } = review;

		if (!this.userId) {
			throw new Meteor.Error('not-authorized');
		}

		Movies.update(movieId, { $push: { reviews: { username, comment, rating } } });
	}
});
