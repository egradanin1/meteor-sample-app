import React from 'react';
import { Router, Route, Switch } from 'react-router';

import createBrowserHistory from 'history/createBrowserHistory';

// route components
import AppContainer from '../imports/ui/appcontainer/AppContainer.js';
import Dashboard from '../imports/ui/dashboard/Dashboard.js';
import Login from '../imports/ui/login/Login.js';
import Register from '../imports/ui/register/Register.js';
import MovieInfo from '../imports/ui/movieinfo/MovieInfo.js';
import TopMovies from '../imports/ui/topmovies/TopMovies.js';
import ChatContainer from '../imports/ui/chats/ChatContainer.js';

const browserHistory = createBrowserHistory();

export default (
	<Router history={browserHistory}>
		<AppContainer>
			<Route exact={true} path="/" component={Dashboard} />
			<Route path="/login" component={Login} />
			<Route path="/register" component={Register} />
			<Route path="/details/:id" component={MovieInfo} />
			<Route path="/topmovies" component={TopMovies} />
			<Route path="/chat" component={ChatContainer} />
		</AppContainer>
	</Router>
);
