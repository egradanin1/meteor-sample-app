# Meteor-Sample-App

A meteor sample app to demonstrate how a web app can be built using a Meteor framework and mongo database with React.

### Building and starting the app ###

To build the app and start meteor, navigate to  the meteor-sample-app directory and run the following commands:

    meteor npm install
	meteor

After that the app starts and it will be accessible on port 3000 and 5 movies will be created in minimongo datatabe.




